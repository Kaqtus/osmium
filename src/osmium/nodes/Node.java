package osmium.nodes;

import java.util.HashMap;
import java.util.List;

public abstract class Node {
    public String tag_name;
    public String content;
    public HashMap<String, String> attrs;
    public List<Node> children;
}
