package osmium.nodes;

public class TextNode extends Node {
    public TextNode(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return String.format("TextNode %s", content);
    }
}
