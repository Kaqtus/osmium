package osmium.nodes;

import java.util.HashMap;
import java.util.List;

public class Element extends Node {
    public Element(String tag_name, HashMap<String, String> attrs, List<Node> children) {
        this.tag_name = tag_name;
        this.attrs = attrs;
        this.children = children;
    }

    @Override
    public String toString() {
        String ret = tag_name;
        if(attrs != null && !attrs.isEmpty()) {
            ret += " "+attrs;
        }
        return ret;
    }
}
