package osmium;

import java.util.Map;

public class Response {
    public String url;
    public int status_code;
    public String content;
    public Map<String, String> headers;

    public Response(String url, int status_code, String content, Map<String, String> headers) {
        this.url = url;
        this.status_code = status_code;
        this.content = content;
        this.headers = headers;
    }
}
