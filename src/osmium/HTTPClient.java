package osmium;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

public class HTTPClient {
    private static final int[] REDIRECT_CODES = {301, 302, 307, 308};

    public Response fetch(String url) throws URISyntaxException, IOException, InterruptedException {
        System.out.println("Fetching "+url);

        URI uri = new URI(url);
        HttpRequest.Builder builder = HttpRequest.newBuilder(uri);
        builder.setHeader("User-Agent", "osmium");
        HttpRequest request = builder.GET().build();

        HttpClient client = HttpClient.newHttpClient();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        Map<String, String> headers = getHeaders(response);

        String redirect = headers.get("location");
        if(Arrays.stream(REDIRECT_CODES).anyMatch(i -> i == response.statusCode()) && redirect != null) {
            redirect = makeAbsolute(url, redirect).toString();
            return fetch(redirect);
        }

        return new Response(url, response.statusCode(), response.body(), headers);
    }

    public URL makeAbsolute(String current_url, String url) {
        if(!url.contains("://"))
            url = current_url + "/" + url;

        try {
            return new URL(url);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Map<String, String> getHeaders(HttpResponse<String> response) {
        Map<String, String> headers = new HashMap<>();

        for (Map.Entry<String, List<String>> h : response.headers().map().entrySet()) {
            headers.put(h.getKey().toLowerCase(), h.getValue().get(0));
        }

        return headers;
    }

    public Response getAboutURL(String url) {
        if(url.equals("about:blank")) {
            return new Response(url, 200, "", Collections.emptyMap());
        }

        return null;
    }
}
