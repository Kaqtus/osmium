package osmium;

import osmium.nodes.Element;
import osmium.nodes.Node;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class DOMInspector extends JFrame {
    DOMInspector(Node root_node) {
        super("DOM Inspector");
        setPreferredSize(new Dimension(400, 600));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        DefaultMutableTreeNode root = buildTree(root_node);

        JTree tree = new JTree(root);
        add(new JScrollPane(tree));
        pack();
        setVisible(true);
    }

    DefaultMutableTreeNode buildTree(Node root) {
        if(root instanceof Element) {
            DefaultMutableTreeNode ret = new DefaultMutableTreeNode(root);
            for (Node child: root.children) {
                ret.add(buildTree(child));
            }
            return ret;
        }else{
            return new DefaultMutableTreeNode(root.content);
        }
    }
}
