package osmium.parsers;

import java.util.HashMap;
import java.util.Map;

public class CSSParser {
    public int pos;
    public String css;

    public CSSParser(String css) {
        this.css = css;
        this.pos = 0;
    }

    public Map<String, String> parse() {
        Map<String, String> declarations = new HashMap<>();
        while(!eof()) {
            String property_name = consumeAlphanumeric();
            if(consume() != ':')
                throw new RuntimeException("Expected ':'");
            consumeWhitespace();

            StringBuilder value = new StringBuilder();
            while(!eof() && nextChar() != ';')
                value.append(consume());

            declarations.put(property_name, value.toString());

            if(nextChar() == ';'){
                consume();
                consumeWhitespace();
            }
        }
        return declarations;
    }

    private String consumeAlphanumeric() {
        StringBuilder tag_name = new StringBuilder();
        while(!eof() && isAlphanumeric(nextChar()))
            tag_name.append(consume());
        return tag_name.toString();
    }

    private char consume() {
        if(eof())
            return '\0';
        char ret = css.charAt(pos);
        pos++;
        return ret;
    }

    private void consumeWhitespace() {
        while(!eof() && Character.isWhitespace(nextChar()))
            consume();
    }

    private char nextChar() {
        if(eof())
            return '\0';
        return css.charAt(pos);
    }

    private boolean isAlphanumeric(char c) {
        return Character.isLetterOrDigit(c) || c == '-' || c == '@';
    }

    private boolean eof() {
        return pos >= css.length();
    }
}
