package osmium.parsers;

import osmium.nodes.Element;
import osmium.nodes.Node;
import osmium.nodes.TextNode;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

// FIXME: navigating stuck at parsing
public class HTMLParser {
    private final String[] VOID_ELEMENTS = {"area", "base", "br", "col", "embed", "hr", "img", "input", "link", "meta", "param", "source", "track", "wbr"};

    public int pos;
    public String html;

    public HTMLParser(String html) {
        this.html = html;
        this.pos = 0;
    }

    public Node parse() {
        List<Node> nodes = parseNodes();

        if(nodes.size() == 1)
            return nodes.get(0);
        return new Element("html", new HashMap<>(), nodes);
    }

    private char nextChar() {
        if(eof())
            return '\0';
        return html.charAt(pos);
    }

    private boolean eof() {
        return pos >= html.length();
    }

    private char consume() {
        if(eof())
            return '\0';
        char ret = html.charAt(pos);
        pos++;
        return ret;
    }

    private boolean startsWith(String s) {
        return html.substring(pos).startsWith(s);
    }

    private void consumeWhitespace() {
        while(!eof() && Character.isWhitespace(nextChar()))
            consume();
    }

    private String consumeAlphanumeric() {
        StringBuilder tag_name = new StringBuilder();
        while(!eof() && isAlphanumeric(nextChar()))
            tag_name.append(consume());
        return tag_name.toString();
    }

    private Node parseNode() {
        if(nextChar() == '<')
            return parseElement();
        else
            return parseTextNode();
    }

    private Node parseTextNode() {
        StringBuilder content = new StringBuilder();
        while(!eof() && nextChar() != '<')
            content.append(consume());

        return new TextNode(content.toString());
    }

    private Node parseElement() {
        _assert(consume() == '<', "");
        String tag_name = consumeAlphanumeric();

        if(tag_name.length() == 0)
            return null;

        if(tag_name.startsWith("!")) {
            ignoreTag();
            return new Element(tag_name, null, Collections.emptyList());
        }

        HashMap<String, String> attrs = parseAttributes();
        _assert(consume() == '>', "");

        if(Arrays.asList(VOID_ELEMENTS).contains(tag_name))
            return new Element(tag_name, attrs, Collections.emptyList());

        List<Node> children = parseNodes();

        if(!eof()) {
            _assert(consume() == '<', "");
            _assert(consume() == '/', "");
            String closing_tag_name = consumeAlphanumeric();
            // _assert(closing_tag_name.equals(tag_name), "Unclosed tag. Expected '"+tag_name+"' got '"+closing_tag_name+"'");
            _assert(consume() == '>', "");
        }

        return new Element(tag_name, attrs, children);
    }

    private SimpleEntry<String, String> parseAttr() {
        String key = consumeAlphanumeric();
        consumeWhitespace();

        String value = "true";
        if(consume() == '=') {
            consumeWhitespace();
            value = parseAttrValue();
        }

        return new SimpleEntry<>(key, value);
    }

    private String parseAttrValue() {
        char quote = consume();
        StringBuilder value = new StringBuilder();

        if(quote == '"' || quote == '\'') {
            while(!eof() && nextChar() != quote)
                value.append(consume());

            _assert(consume() == quote, "");
        }else{
            value.append(quote);
            while(!eof() && nextChar() != ' ' && nextChar() != '>')
                value.append(consume());
        }

        return value.toString();
    }

    private HashMap<String, String> parseAttributes() {
        HashMap<String, String> attrs = new HashMap<>();

        while(true) {
            consumeWhitespace();
            if(nextChar() == '/')
                consume();
            if(nextChar() == '>')
                break;

            SimpleEntry<String, String> attr = parseAttr();
            attrs.put(attr.getKey(), attr.getValue());
        }

        return attrs;
    }

    private List<Node> parseNodes() {
        List<Node> nodes = new ArrayList<>();

        while(true) {
            consumeWhitespace();
            if(eof() || startsWith("</"))
                break;

            Node node = parseNode();
            if(node != null)
                nodes.add(node);
        }

        return nodes;
    }

    private boolean isAlphanumeric(char c) {
        return Character.isLetterOrDigit(c) || c == '!' || c == '-' || c == ':';
    }

    private void ignoreTag() {
        while(!eof() && nextChar() != '>')
            consume();
        consume();
    }


    public void _assert(boolean expr, String message) {
        if(message.length() == 0)
            message = "Unknown error";

        if(!expr) {
            System.out.println("Error! Last 30 chars: "+html.substring(pos-30, pos)+" Current char: "+nextChar());
            throw new RuntimeException(String.format("%s at %d\n", message, pos));
        }
    }
}
