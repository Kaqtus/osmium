package osmium;

import osmium.nodes.Element;
import osmium.nodes.Node;
import osmium.nodes.TextNode;
import org.unbescape.html.HtmlEscape;
import osmium.parsers.CSSParser;
import osmium.parsers.HTMLParser;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.*;

public class MyFrame extends JFrame {
    private static final String HOMEPAGE = "serenityos.org";
    private static final String[] TEXTNODE_BLACKLIST = {"style", "script", "title", "head"};
    private static final String[] NEWLINE_BEFORE = {"h1", "h2", "h3", "h4", "h5", "h6", "big", "p"};
    private static final String[] NEWLINE_AFTER = {"h1", "h2", "h3", "h4", "h5", "h6", "big", "ul", "li", "p", "tr", "li"};
    Map<String, String> COLOR_MAP  = new HashMap<>() {{
        put("black", "#000000");
        put("silver", "#C0C0C0");
        put("gray", "#808080");
        put("white", "#FFFFFF");
        put("maroon", "#800000");
        put("red", "#FF0000");
        put("purple", "#800080");
        put("fuchsia", "#FF00FF");
        put("green", "#008000");
        put("lime", "#00FF00");
        put("olive", "#808000");
        put("yellow", "#FFFF00");
        put("navy", "#000080");
        put("blue", "#0000FF");
        put("teal", "#008080");
        put("aqua", "#00FFFF");
    }};

    private final JTextField urlbar;
    private final JLabel statusbar;
    private final JPanel panel;
    private final HTTPClient client;

    private JPanel line;
    private String current_url;
    private Node root_node;
    private final java.util.List<String> history;

    public MyFrame() {
        super("Osmium");

        client = new HTTPClient();
        history = new ArrayList<>();
        setPreferredSize(new Dimension(800, 500));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new BorderLayout());

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setResizeWeight(0.03);
        split.setEnabled(true);
        split.setDividerSize(0);

        JSplitPane split2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split2.setResizeWeight(0.01);
        split2.setEnabled(true);
        split2.setDividerSize(2);
        split2.setBackground(Color.WHITE);

        JPanel navpanel = new JPanel();
        navpanel.setBackground(Color.WHITE);
        navpanel.setLayout(new BoxLayout(navpanel, BoxLayout.X_AXIS));

        Font font = new Font("Times New Roman", Font.BOLD, 20);

        JButton back_button = new JButton("←");
        back_button.setFont(font);
        back_button.setBackground(Color.WHITE);
        back_button.setBorderPainted(false);
        back_button.addActionListener(e -> {
            if(history.size() < 2)
                return;

            int i = history.size() - 2;
            String prev_url = history.get(i);
            history.remove(i);

            history.remove(history.size() - 1);
            navigate(prev_url);
        });
        navpanel.add(back_button);

        JButton refresh_button = new JButton("⟳");
        refresh_button.setFont(font);
        refresh_button.setBackground(Color.WHITE);
        refresh_button.setBorderPainted(false);
        refresh_button.addActionListener(e -> navigate(current_url));
        navpanel.add(refresh_button);

        JButton home_button = new JButton("⌂");
        home_button.setFont(font);
        home_button.setBackground(Color.WHITE);
        home_button.setBorderPainted(false);
        home_button.addActionListener(e -> navigate(HOMEPAGE));
        navpanel.add(home_button);

        urlbar = new JTextField();
        urlbar.setFont(new Font("Times New Roman", Font.PLAIN, 11));
        urlbar.addActionListener(e -> navigate(urlbar.getText()));
        navpanel.add(urlbar);

        statusbar = new JLabel();

        split2.add(navpanel);
        split2.add(statusbar);
        split.add(split2);

        panel = new JPanel();
        panel.setBackground(Color.WHITE);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JScrollPane scroll = new JScrollPane(panel);
        scroll.getVerticalScrollBar().setUnitIncrement(8);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        split.add(scroll);

        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F5"), "f5");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F12"), "f12");
        panel.getActionMap().put("f5", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                navigate(current_url);
            }
        });
        panel.getActionMap().put("f12", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DOMInspector(root_node);
            }
        });

        add(split, BorderLayout.CENTER);
        pack();
        setVisible(true);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        navigate(HOMEPAGE);
    }

    // TODO: kill previous thread if already rendering
    public void navigate(String url) {
        new Thread(() -> _navigate(url)).start();
    }

    private void _navigate(String url) {
        long start_time = new Date().getTime();
        status("Navigating to: " + url);

        history.add(url);
        urlbar.setText(url);

        Response response = client.getAboutURL(url);

        if(response == null) {
            if(!url.contains("://")){
                navigate("http://"+url);
                return;
            }

            if(url.split("#")[0].equals(current_url) && url.contains("#"))
                return;

            try {
                response = client.fetch(url);
            } catch (Throwable e) {
                error(e);
                return;
            }
        }else{
            status("Loading about URL: " + url);
        }

        current_url = response.url;
        urlbar.setText(current_url);

        String content_type = response.headers.get("content-type");

        if(content_type != null && content_type.startsWith("image/")){
            showImage(response);
        }else{
            status("Parsing...");
            try{
                HTMLParser parser = new HTMLParser(response.content);
                root_node = parser.parse();
            } catch (Throwable e) {
                error(e);
                status(e.toString());
                return;
            }

            status("Rendering...");
            panel.removeAll();
            newLine();
            render(root_node, null);
            newLine();
        }

        status(String.format("Done in %d ms", new Date().getTime() - start_time));
    }

    private void render(Node root, Node parent) {
        System.out.println("Processing: "+root);

        if(root instanceof TextNode) {
            if(Arrays.asList(TEXTNODE_BLACKLIST).contains(parent.tag_name))
                return;

            String content = root.content;
            if(!parent.tag_name.equals("pre"))
                content = content.strip().replace("\n", " ");
            if(parent.tag_name.equals("li"))
                content = "• "+content;

            content = HtmlEscape.unescapeHtml(content);
            JLabel label = new JLabel(content+" ");
            boolean append_label = true;

            Font font = new Font("Times New Roman", Font.PLAIN, 16);
            Map<TextAttribute, Object> fonta = new HashMap<>();

            System.out.println("Rendering TextNode: '"+content+"' parent: "+ parent.tag_name);

            // TODO: wrap long lines
            switch (parent.tag_name) {
                case "h1", "big" -> {
                    fonta.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_EXTRABOLD);
                    fonta.put(TextAttribute.SIZE, 32F);
                }
                case "h2" -> {
                    fonta.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_EXTRABOLD);
                    fonta.put(TextAttribute.SIZE, 24F);
                }
                case "h3" -> {
                    fonta.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_EXTRABOLD);
                    fonta.put(TextAttribute.SIZE, 18F);
                }
                case "h4", "b", "strong" -> fonta.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_EXTRABOLD);
                case "i", "em" -> font = new Font("Times New Roman", Font.ITALIC, 16);
                case "u", "ins" -> fonta.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                case "strike", "del" -> fonta.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
                case "a" -> {
                    label.setText("<html><a href=\"\">" + content + "</a></html>");
                    label.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            String url = parent.attrs.get("href");
                            url = client.makeAbsolute(current_url, url).toString();
                            navigate(url);
                        }
                    });
                }
                case "button" -> {
                    append(new JButton(content));
                    append_label = false;
                }
            }

            if(append_label){
                label.setFont(font.deriveFont(fonta));
                String style = parent.attrs.get("style");
                if(style != null){
                    CSSParser css_parser = new CSSParser(style);
                    applyCSS(label, css_parser.parse());
                }
                append(label);
            }
            if(parent.tag_name.equals("pre") && content.endsWith("\n"))
                newLine();
        }

        if(root instanceof Element) {
            if (Arrays.asList(NEWLINE_BEFORE).contains(root.tag_name))
                newLine();

            switch (root.tag_name) {
                case "br" -> newLine();
                case "title" -> {
                    if(root.children.size() == 0)
                        break;
                    setTitle(root.children.get(0).content);
                }
                case "img" -> {
                    if (!root.attrs.containsKey("src") || root.attrs.get("src").length() == 0)
                        break;

                    // FIXME: ImageIO.read doesn't follow redirects
                    URL url = client.makeAbsolute(current_url, root.attrs.get("src"));
                    try {
                        BufferedImage img = ImageIO.read(url);
                        if(img == null)
                            throw new Exception("img is null");
                        append(new JLabel(new ImageIcon(img)));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        append(new JLabel("Failed to load " + root.attrs.get("src") + " (" + url + ")"));
                        newLine();
                    }
                }
            }

            for (Node child : root.children)
                render(child, root);

            if(Arrays.asList(NEWLINE_AFTER).contains(root.tag_name))
                newLine();
        }

        panel.revalidate();
        panel.repaint();
    }

    private JLabel applyCSS(JLabel c, Map<String, String> declarations) {
        for (Map.Entry<String, String> e : declarations.entrySet()) {
            if(e.getKey().equals("color"))
                c.setForeground(getColor(e.getValue()));
        }
        return c;
    }

    private Color getColor(String color) {
        if(COLOR_MAP.containsKey(color))
            color = COLOR_MAP.get(color);

        if(color.startsWith("#"))
            return Color.decode(color);

        return null;
    }

    private void showImage(Response response) {
        try {
            URL urlo = new URL(response.url);
            BufferedImage img = ImageIO.read(urlo);

            setTitle(current_url);
            panel.removeAll();
            panel.add(new JLabel(new ImageIcon(img)));
        } catch (Throwable e) {
            error(e);
        }
    }

    private void append(Component c) {
        line.add(c);
    }

    private void newLine() {
        if(line != null){
            line.setMaximumSize(line.getPreferredSize());
            panel.add(line);
        }
        line = new JPanel();
        line.setBackground(Color.WHITE);
        line.setLayout(new BoxLayout(line, BoxLayout.LINE_AXIS));
        line.setAlignmentX(Component.LEFT_ALIGNMENT);
    }

    private void status(String s) {
        if(s.length() > 0)
            System.out.println(s);
        statusbar.setText(s);
    }

    private void error(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        String exc_str = sw.toString();
        String exc = exc_str.substring(0, Math.min(exc_str.length(), 1500));
        EventQueue.invokeLater(() -> JOptionPane.showMessageDialog(null, exc));
    }
}
