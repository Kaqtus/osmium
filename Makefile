UNBESCAPE = out/unbescape-1.1.6.RELEASE/dist/unbescape-1.1.6.RELEASE.jar

build: ${UNBESCAPE}
	javac -cp "${UNBESCAPE}" -d out/osmium/ -sourcepath src/ src/osmium/Main.java

${UNBESCAPE}:
	mkdir -p out/osmium
	wget -O out/unbescape.zip https://github.com/unbescape/unbescape/releases/download/unbescape-1.1.6.RELEASE/unbescape-1.1.6.RELEASE-dist.zip
	unzip -o -d out/ out/unbescape.zip

run: build
	java -cp "out/osmium:${UNBESCAPE}" osmium/Main
