# osmium
![](https://tokei.rs/b1/github/Kaqtus14/osmium)

Web browser written from scratch in Java

## Build
```bash
make run
```

## Dependencies
* [unbescape](https://github.com/unbescape/unbescape)
